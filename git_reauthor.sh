#! /usr/bin/env bash

function reauthor_all {
  local old_email
  local new_email
  local new_name

  if [ $# -eq 3 ]; then
    old_email="$1"
    new_email="$2"
    new_name="$3"
  else
    read -rp "Old user email: " old_email
    read -rp "New user email: " new_email
    read -rp "New user name: " new_name
  fi

  if [[ "$old_email" == "" || "$new_email" == "" || "$new_name" == "" ]]; then return; fi

  git config user.email "$new_email"
  git config user.name "$new_name"

  local command
  command='[[ \"$(git log -1 --pretty=format:'%ae')\" == '
  command+="'$old_email' ]] && git commit --amend --author '$new_name <$new_email>' --no-edit || true"

  git rebase -i --root --committer-date-is-author-date -x "$command"
}

reauthor_all "$@"
