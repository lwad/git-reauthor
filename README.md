# Git Reauthor

When migrating from one remote repo to another, you might use a different git email address / username.

If that's you - fret not! You can change the email address and username on your commits with this handy script.

## Usage

### Without Arguments

1. Run the `git_reauthor.sh` script.
2. Provide the email address that was on your old commits. 
3. Provide your new email address and username.

### With Arguments

1. Run the `git_reauthor.sh` script with these arguments in order:
   - old email address 
   - new email address
   - new username

That's it! If you're not moving to a new service, but you just want to change your email/username, you'll need to force push.
